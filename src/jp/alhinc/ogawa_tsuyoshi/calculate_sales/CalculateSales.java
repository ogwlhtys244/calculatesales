package jp.alhinc.ogawa_tsuyoshi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		/* ◇エラーチェック コマンドライン引数があるかの確認-------------------*/
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		//	●1.支店定義ファイル読み込み----------------------------------------------------
		Map<String, String> branchNames = new HashMap<>(); /*  支店情報(支店コード,支店名)  */
		Map<String, Long> branchSales = new HashMap<>(); /*  売上情報(支店コード,売上金額)  */

//		branch 読込みメソッド
		if (!inputFile(args[0], "branch.lst", "^[0-9]{3}", "支店", branchNames, branchSales)) {
			return;
		}

		//	●2.商品定義ファイル読み込み----------------------------------------------------
		Map<String, String> commodityNames = new HashMap<>(); /*  支店情報(商品コード,商品名)  */
		Map<String, Long> commoditySales = new HashMap<>(); /*  売上情報(商品コード,売上金額)  */

//		commodity 読込みメソッド
		if (!inputFile(args[0], "commodity.lst", "^[A-Za-z0-9]+$", "商品", commodityNames, commoditySales)) {
			return;
		}

		//	●3.集計----------------------------------------------------------------------
		File[] files = new File(args[0]).listFiles(); /* files=フォルダ内全部 */
		List<File> rcdFiles = new ArrayList<>();
		BufferedReader br = null;

		for (int i = 0; i < files.length; i++) { /*売上ファイルのみを抽出*/
			String fileName = files[i].getName();

			/* ◇エラーチェック ファイルかディレクトリかの確認---------------------------*/
			if (files[i].isFile() && fileName.matches("^[0-9]{8}.+rcd$")) { /* rcd 且つ数字8桁を抽出 */
				rcdFiles.add(files[i]); /* 0-9の8桁rcdファイルを追加 */
			}
		}
		Collections.sort(rcdFiles);

		/* ◇エラーチェック 連番の確認---------------------------------------------------*/
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			if ((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		for (int i = 0; i < rcdFiles.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				ArrayList<String> extraction = new ArrayList<>(); /*  extraction<> rcdFilesの中身リスト */
				String line;
				while ((line = br.readLine()) != null) { /*  売上ファイル読込  */
					extraction.add(line);
				}
				/* ◇エラーチェック 売上ファイルの中身が3行か ----------------------------------*/
				if (extraction.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}
				String applicableFile = rcdFiles.get(i).getName();
				String branchCodeEx = extraction.get(0); /* 支店コード */
				String comoddityCode = extraction.get(1); /* 商品コード */
				String branchSaleEx = extraction.get(2); /* 売上額 */

				/* ◇エラーチェック 売上金額が数字なのか確認する方法 ---------------------------*/
				if (!branchSaleEx.matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				/* ◇エラーチェック 売上ファイルの支店コードが支店定義ファイルに存在するか -----*/
				if (!branchNames.containsKey(branchCodeEx)) {
					System.out.println(applicableFile + "の支店コードが不正です");
					return;
				}
				/* ◇エラーチェック 売上ファイルの商品コードが商品定義ファイルに存在するか -----*/
				if (!commodityNames.containsKey(comoddityCode)) {
					System.out.println(applicableFile + "の商品コードが不正です");
					return;
				}

				long fileSale = Long.parseLong(branchSaleEx); /*  StringをLongへ変換し売り上げとする  */
				Long saleAmount = branchSales.get(branchCodeEx) + fileSale; /*  Keyを引数にValueを取得し売上へ加算  */

				branchSales.put(branchCodeEx, saleAmount); /*  Mapへ返す getの0番目キーのvalueに入れる  */

				Long comoddityAmount = commoditySales.get(comoddityCode) + fileSale; /*  Keyを引数にValueを取得し売上へ加算  */

				commoditySales.put(comoddityCode,
						comoddityAmount); /*  Mapへ返す getの0番目キーのvalueに入れる  *

											/* ◇エラーチェック  10桁に売り上げがおさまっているか---------------------------*/
				if (saleAmount >= 10000000000L || comoddityAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}

//			branch 集計出力
			if (!outputFile(args[0], "branch.out", branchNames, branchSales)) {
				return;
			}

//			commodity 集計出力
			if (!outputFile(args[0], "commodity.out", commodityNames, commoditySales)) {
				return;
			}
		}
	}

//	ファイル読込メソッド
	private static boolean inputFile(String args, String inFileName, String matchesCode, String setFile,Map<String, String> names,
			Map<String, Long> sales) {
		BufferedReader br = null;
		try {
			File file = new File(args, inFileName); /*  売上定義ファイルの展開  */

			/* ◇エラーチェック fileが存在しているかの確認---------------------*/
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) { /*  readLineは１行ずつ読み込むメソッド  */
				String[] items = line.split(","); /*  "," 区切りで分割　*/

				/* ◇エラーチェック フォーマットが不正であるかの確認-------------------*/
				if ((items.length != 2) || (!items[0].matches(matchesCode))) {
					System.out.println(setFile + "定義ファイルのフォーマットが不正です");
					return false;
				}
				names.put(items[0], items[1]); /*  items[0]:コード、items[1]:名  */
				sales.put(items[0], 0L); /*  items[0]:コード、items[]:売上金額  */
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
			return true;
	}

//	集計出力メソッド
	private static boolean outputFile(String args, String outFileName, Map<String, String> names,
			Map<String, Long> sales) {
		//		●4.集計結果出力----------------------------------------------------------------------

		//		4-1.支店別集計出力
		BufferedWriter bw = null;
		try {
			File file = new File(args, outFileName);
			FileWriter fw = new FileWriter(file);

			bw = new BufferedWriter(fw);
			for (String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
